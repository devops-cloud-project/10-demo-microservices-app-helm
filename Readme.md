## Ways to run microservices K8s/Helm
Pre-requisities:
- create Kubernetes cluster on Linode
- download kubeconfig.yaml 
- command: 'chmod x+u kubeconfig.yaml'
- command: 'export KUBECONFIG=kubeconfig.yaml'

### Kubernetes (config.yaml)
- kubectl apply -f config.yaml
- kubectl delete -f config.yaml

### Helm
Single chart:
- helm install -f values/frontend-values.yaml frontend charts/frontend-chart/
- helm uninstall -f values/frontend-values.yaml frontend charts/frontend-chart/

Multiple charts:
- ./install.sh
- ./uninstall.sh

### Helmfile (helmfile.yaml)
- helmfile sync
- helmfile destroy